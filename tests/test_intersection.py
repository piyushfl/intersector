import sys
import glob
from time import time
from pathlib import Path
import pytest
import numpy as np

#sys.path.append("..")
from intersector.intersector import intersect
import json
from numpy.testing import assert_almost_equal
from pprint import pprint

_FDIR = Path(__file__).resolve().parent
TEST_FNAMES = glob.glob(str(_FDIR) + "/**/*.json")
TOT_TIME = 0.0

def interval_equality(l1, l2):
    """
    takes two sets of intervals and computes their distance
    sample
    l1 = [ ((0,0), (1,0)) ]
    l2 = [ ((0.001,0), (1,0)) ]
    """
    assert len(l1) == len(l2), "FATAL: The number of elements in the outputs Mismatch"
    for i, j in zip(l1, l2):
        p1, p2 = i
        q1, q2 = j
        assert_almost_equal(p1, q1)
        assert_almost_equal(p2, q2)


def test_square_1():
    unit_square = [(0, 0), (1, 0), (1, 1), (0, 1)]
    output = intersect(unit_square, a_val=1, b_val=0, c_val=0)
    assert len(output) == 1


def _load_float(s):
    print("Input Float:", s)
    print("Output Float:", np.float64(s))
    return np.float64(s)

@pytest.mark.parametrize("file_path", TEST_FNAMES)
def test_single_json(file_path):
    global TOT_TIME
    print("New Proccessing: ", file_path)
    tdata = open(file_path).read()
    # data = json.loads(tdata, parse_float=_load_float)
    data = json.loads(tdata, parse_float=np.float64)
    # Feed the input from data to intersect function and check its output
    print(data)
    output = data["output"]
    data.pop("output", None)
    start_time = time()
    coutput = intersect(**data)
    TOT_TIME += time() - start_time
    print("Testing Approximate Equality:")
    pprint(output)
    pprint(coutput)
    print("=" * 40)
    interval_equality(output, coutput)
    print(f"Current elapsed computation time {TOT_TIME} seconds for {len(TEST_FNAMES)} files")
